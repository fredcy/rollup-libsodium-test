# Using libsodium-wrappers module via Rollup

This is an SSCCE of problems building a browser app via Rollup that imports the libsodium-wrappers module.

```
npm install
npm run build
npm run serve
```

Visit the served page and view the console.

I expect just a "Hello" message but get instead a TypeError: "A is undefined".
