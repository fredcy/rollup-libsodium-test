import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import nodePolyfills from 'rollup-plugin-node-polyfills';

export default {
    input: "src/test1.js",
    
    output: {
	file: "dist/bundle.js",
	format: "iife",
    },

    plugins: [
	commonjs(),
	nodePolyfills(),
	resolve(),
    ],

    watch: {
	clearScreen: false,
    },
};
